//
//  UserState.swift
//  NavigationTask
//
//  Created by mdubkov on 04.04.2022.
//

import SwiftUI

enum AppState {
    case authorize
    case unauthorize
}
