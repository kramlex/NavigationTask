//
//  ContentView.swift
//  NavigationTask
//
//  Created by mdubkov on 04.04.2022.
//

import SwiftUI

struct ContentView: View {
    
    @State private var state: AppState = .unauthorize
    
    var body: some View {
        switch state {
        case .unauthorize:
            NavigationView {
                AuthView(appState: $state)
            }
        case .authorize:
            HomeTabView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
