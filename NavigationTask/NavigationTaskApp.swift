//
//  NavigationTaskApp.swift
//  NavigationTask
//
//  Created by mdubkov on 04.04.2022.
//

import SwiftUI

@main
struct NavigationTaskApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
