//
//  WalletView.swift
//  NavigationTask
//
//  Created by mdubkov on 04.04.2022.
//

import SwiftUI

struct WalletView: View {
    
    @Binding var walletIsActive: Bool
    
    var body: some View {
        VStack {
            Text("1222 RUB")
                .font(.title)
                .foregroundColor(.gray)
            NavigationLink {
                AddCashView(walletIsActive: $walletIsActive)
            } label: {
                Text("Add Cash")
            }
        }
        .navigationTitle("Wallet")
    }
}
