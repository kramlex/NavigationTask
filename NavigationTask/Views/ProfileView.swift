//
//  ProfileView.swift
//  NavigationTask
//
//  Created by mdubkov on 04.04.2022.
//

import SwiftUI

struct ProfileView: View {
    
    @State private var walletIsActive: Bool = false
    
    var body: some View {
        NavigationView {
            NavigationLink(isActive: $walletIsActive) {
                WalletView(walletIsActive: $walletIsActive)
            } label: {
                Button {
                    walletIsActive.toggle()
                } label: {
                    Text("Open Wallet")
                }
            }
            .navigationTitle("Profile")
        }
    }
}
