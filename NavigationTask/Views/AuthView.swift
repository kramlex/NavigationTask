//
//  AuthView.swift
//  NavigationTask
//
//  Created by mdubkov on 04.04.2022.
//

import SwiftUI

struct AuthView: View {
    
    @Binding var appState: AppState
    
    var body: some View {
        NavigationLink {
            SecondAuthView(appState: $appState)
        } label: {
            Text("final first auth step")
        }
    }
}

struct SecondAuthView: View {

    @Binding var appState: AppState
    
    var body: some View {
        VStack {
            Button {
                appState = .authorize
            } label: {
                Text("final second auth step")
            }

        }
    }
}


struct AuthView_Previews: PreviewProvider {

    @State private var state: AppState = .unauthorize
    
    static var previews: some View {
        let previewView = AuthView_Previews()
        Group {
            AuthView(appState: previewView.$state)
        }
    }
}
