//
//  HomeTabView.swift
//  NavigationTask
//
//  Created by mdubkov on 04.04.2022.
//

import SwiftUI

struct HomeTabView: View {
    
    enum Tab {
        case profile
        case setting
    }
    
    @State private var selectedTab: Tab = .profile
    
    var body: some View {
        TabView(selection: $selectedTab) {
            ProfileView()
                .tabItem {
                    Image(systemName: "briefcase")
                    Text("Profile")
                }
                .tag(Tab.profile)
            
            SettingsView()
                .tabItem {
                    Image(systemName: "gear")
                    Text("Settings")
                }
                .tag(Tab.setting)
        }
    }
}



struct SettingsView: View {
    var body: some View {
        NavigationView {
            Text("Settings")
                .navigationTitle("Settings")
        }
    }
}
