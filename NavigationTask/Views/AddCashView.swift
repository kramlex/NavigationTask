//
//  AddCashView.swift
//  NavigationTask
//
//  Created by mdubkov on 04.04.2022.
//

import SwiftUI

struct AddCashView: View {
    
    @Binding var walletIsActive: Bool
    @State private var text: String = ""
    
    var body: some View {
        Group {
            Text("Add Cash")
            TextField("Money", text: $text)
            Button {
                //
            } label: {
                Text("Add Cash")
            }
            Button {
                walletIsActive.toggle()
            } label: {
                Text("Back to profile")
            }

        }
    }
}
